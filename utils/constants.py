from ctypes import c_void_p, Structure
from ctypes.wintypes import DWORD, USHORT, UINT

MEM_COMMIT = 0x1000
MEM_FREE = 0x10000
MEM_RESERVE = 0x2000

PAGE_EXECUTE = 0x10
PAGE_EXECUTE_READ = 0x20
PAGE_EXECUTE_READ_WRITE = 0x40
PAGE_EXECUTE_WRITE_COPY = 0x80
PAGE_NO_ACCESS = 0x01
PAGE_READ_WRITE = 0x04
PAGE_WRITE_COPY = 0x08

MEM_IMAGE = 0x1000000
MEM_MAPPED = 0x40000
MEM_PRIVATE = 0x20000

PROCESS_ALL_ACCESS = 0x1F0FFF
PROCESS_QUERY_INFORMATION = 0x0400
PROCESS_WM_READ = 0x0010
PROCESS_VM_OPERATION = 0x0008
PROCESS_VM_WRITE = 0x0020


class MemoryBasicInformation(Structure):
    _fields_ = [
        ("BaseAddress", c_void_p),
        ("AllocationBase", c_void_p),
        ("AllocationProtect", DWORD),
        ("RegionSize", UINT),
        ("State", DWORD),
        ("Protect", DWORD),
        ("Type", DWORD)
    ]


class SystemInfo(Structure):
    _fields_ = [
        ("wProcessorArchitecture", USHORT),
        ("wReserved", USHORT),
        ("dwPageSize", UINT),
        ("lpMinimumApplicationAddress", UINT),
        ("lpMaximumApplicationAddress", UINT),
        ("dwActiveProcessorMask", UINT),
        ("dwNumberOfProcessors", UINT),
        ("dwProcessorType", UINT),
        ("dwAllocationGranularity", UINT),
        ("wProcessorLevel", USHORT),
        ("wProcessorRevision", USHORT)
    ]
