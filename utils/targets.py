from ctypes import (
    c_int,
    sizeof,
)
from struct import calcsize, Struct


class IntValue(object):

    ASSUME_LEVEL = True
    BYTE_ORDER = "little"

    def __init__(self, value):
        self.level = 1
        self.value = value
        self.ctype = c_int(value)
        self.size = sizeof(self.ctype)
        self.bytes = (value).to_bytes(self.size, byteorder=self.BYTE_ORDER)
        self.type = self.ctype._type_
        if hasattr(self.type, '_type_'):
            self.type = self.type._type_
        if self.type == 'c':
            self.type = 's'
        elif self.ASSUME_LEVEL:
            divider = 1
            for _ in range(self.size):
                divider *= 2
                if not self.size % divider:
                    self.level = divider
        self.type_size = calcsize(self.type)

    @property
    def packer(self):
        length = self.size // self.type_size
        format  = str(length) + str(self.type)
        return Struct(format)
