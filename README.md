# Memory Scanner

Memory Scanner written with Python 3.5. 
It includes modules ctypes and ctypes.wintypes. It does not need wingui32 or other external libraries.

#### Warning

@Tested on Windows 10

```bash
Version 1.0.0
Python version 3.5
used modules: ctypes, ctypes.wintypes, re, struct
warning: it supports only READ, and reading int values.
```

## Getting Started

Memory Scanner is easy to use. Import the class MemoryScanner and use it with **with** statement. Class will take care of closing opened process.

### scanning chunks

When process starts, the system allocates memory for its heap, stack and regions. Windows tries allocate
any free memory available for the User-Mode wherever it wants, that's why we do not have continuous allocation.
We need to scan memory and find all chunks which belongs to our process (pid).

When we have chunks in a list, we can check values there with two methods. Results are the same. 
* scan_chunk_slow - which uses python for loop, range and unpacking bytes to compare values
This solution is good to pick up for a moment, and try understand how it get values.
* scan_chunk - default method, which does operation only on bytes. Very fast!

### Example

```python
from ms import MemoryScanner
from utils.targets import IntValue

if __name__ == '__main__':
    # example pid
    pid = 2904
    with MemoryScanner(pid) as memory:
        # collect all chunks which belongs to given pid
        chunks = memory.get_chunks()

        # initialize IntValue. We want to search in memory example value
        example_value = 2921 # my bot character's HP
        int_value = IntValue(example_value)

        # scan each chunk and search the value
        results = []
        for chunk in chunks:
            res = memory.scan_chunk(chunk, int_value)
            results.extend(res)
        print("done")
        print(results)
```

Example output:

```python
done
[('0xb4c089f', 2921), ('0xb4c08a7', 2921), ('0x4988cb0', 2921), ('0xf89cbe8', 2921), ...]
```

## How to Install

Clone my repository and have fun!

```bash
git clone https://github.com/marekgancarz/memoryScanner.git
```

## Authors

* **Marek Gancarz** - *Initial work* - [marekgancarz](https://github.com/marekgancarz)

## Features

* Scan memory to get values directly from it.
* Very good choice for botting and scaning values (hp/mp) from memory.
