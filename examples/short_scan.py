from ms import MemoryScanner
from utils.targets import IntValue

if __name__ == '__main__':
    # example pid
    pid = 2904
    with MemoryScanner(pid) as memory:
        # collect all chunks which belongs to given pid
        chunks = memory.get_chunks()

        # initialize IntValue. We want to search in memory example value
        example_value = 2921
        int_value = IntValue(example_value)

        # scan each chunk and search the value
        results = []
        for chunk in chunks:
            res = memory.scan_chunk(chunk, int_value)
            results.extend(res)
        print("done")
        print(results)
