#!/usr/bin/python3.5
import re
from ctypes import (
    byref,
    sizeof,
    windll,
    c_buffer,
    c_uint,
    c_ulong
)
from .utils.constants import (
    PROCESS_WM_READ,
    PROCESS_VM_OPERATION,
    PROCESS_VM_WRITE,
    PROCESS_QUERY_INFORMATION,
    MEM_PRIVATE,
    MEM_COMMIT,
    PAGE_EXECUTE_READ_WRITE,
    PAGE_EXECUTE_READ,
    PAGE_READ_WRITE,
    SystemInfo,
    MemoryBasicInformation,
)


class MemoryScanner(object):

    OPEN_PROCESS = windll.kernel32.OpenProcess
    READ_PROCESS_MEMORY = windll.kernel32.ReadProcessMemory
    CLOSE_HANDLE = windll.kernel32.CloseHandle
    VIRTUAL_QUERY_EX = windll.kernel32.VirtualQueryEx
    GET_SYSTEM_INFO = windll.kernel32.GetSystemInfo
    ACCESS = PROCESS_WM_READ | PROCESS_VM_OPERATION | PROCESS_VM_WRITE | PROCESS_QUERY_INFORMATION

    def __init__(self, pid):
        self.pid = int(pid)
        self.memory_handler = self.OPEN_PROCESS(self.ACCESS, False, self.pid)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def read(self, memory_address):
        """
        Read int value from given memory_address.

        :param memory_address: example format: 0x00000000
        :return: int value from given memory address
        """
        if not memory_address:
            raise ValueError("No given memory address. Format: 0x00000000")
        if isinstance(memory_address, str):
            memory_address = int(memory_address, 16)
        content = c_uint()
        buffer = byref(content)
        buffer_size = sizeof(content)
        bytes_read = c_ulong(0)
        self.READ_PROCESS_MEMORY(self.memory_handler, memory_address, buffer, buffer_size, bytes_read)
        return content.value

    def get_chunks(self):
        """
        Method scans almost every possible address and check if it belongs to the given process (pid).
        We collect list of tuples where tuple represents a chunk, with base_address and chunk_size.

        Description of the problem:
        When process starts, the system allocates memory for its heap, stack and regions. Windows tries allocate
        any free memory available for the User-Mode wherever it wants, that's why we do not have continuous allocation.
        We need to scan memory and find all chunks.

        :return: list of tuples, sorted by chunk's size.
        """
        system_info = SystemInfo()
        self.GET_SYSTEM_INFO(byref(system_info))
        min_address = system_info.lpMinimumApplicationAddress
        max_address = system_info.lpMaximumApplicationAddress

        chunk_start_address = min_address
        chunks = []
        while chunk_start_address < max_address:
            mbi = MemoryBasicInformation()
            self.VIRTUAL_QUERY_EX(self.memory_handler, chunk_start_address, byref(mbi), sizeof(mbi))
            base_address = mbi.BaseAddress
            chunk_size = mbi.RegionSize
            next_region = base_address + chunk_size

            if (mbi.Type == MEM_PRIVATE and mbi.State == MEM_COMMIT and
                mbi.Protect in [PAGE_EXECUTE_READ, PAGE_EXECUTE_READ_WRITE, PAGE_READ_WRITE]):
                chunks.append((base_address, chunk_size))

            chunk_start_address = next_region
        return sorted(chunks, key=lambda x: x[1])

    def scan_chunk_slow(self, chunk, int_value):
        """
        Scan given chunk to find there value given from object IntValue.
        Basic method with for loop. It's good for better understanding how script searches values from memory.
        Very slow method, because it uses python's loop with range and bytes are unpacked during iteration.

        :param chunk: tuple, base_address with chunk_size
        :param int_value: object IntValue
        :return: list of tuples, where tuple is a (hex, int value).
        """
        base_address, region_size = chunk
        buffer = c_buffer(region_size)
        self.READ_PROCESS_MEMORY(self.memory_handler, base_address, buffer, region_size, c_ulong(0))
        chunk_bytes = buffer.raw
        results = []
        for i in range(0, (region_size - int_value.size), int_value.level):
            partial = chunk_bytes[i:i + int_value.size]
            if int_value.packer.unpack(partial)[0] == int_value.value:
                results.append((hex(base_address + i), int_value.value))
        del chunk_bytes
        return results

    def scan_chunk(self, chunk, int_value):
        """
        Default scan method for chunks. It scans given chunk to find inside it the value, given from object IntValue.
        Method does operation on bytes. It's very fast!

        :param chunk: tuple, base_address with chunk_size
        :param int_value: object IntValue
        :return: list of tuples, where tuple is a (hex, int value).
        """
        base_address, region_size = chunk
        buffer = c_buffer(region_size)
        self.READ_PROCESS_MEMORY(self.memory_handler, base_address, buffer, region_size, c_ulong(0))
        chunk_bytes = buffer.raw
        results = []
        if int_value.bytes in chunk_bytes:
            offsets = [m.start() for m in re.finditer(int_value.bytes, chunk_bytes)]
            for offset in offsets:
                results.append((hex(base_address + offset), int_value.value))
        del chunk_bytes
        return results

    def get_chunk_bytes(self, chunk):
        """
        Read buffer from given chunk.

        :param chunk: tuple, base_address with chunk_size
        :return: bytes
        """
        base_address, region_size = chunk
        buffer = c_buffer(region_size)
        self.READ_PROCESS_MEMORY(self.memory_handler, base_address, buffer, region_size, c_ulong(0))
        return buffer.raw

    def close(self):
        self.CLOSE_HANDLE(self.memory_handler)
